import Axios from "axios";

/**
 * Send request to an endpoints
 * @param {String} url Request url
 * @param {String} method Method (request type)
 */
const sendRequestToBackend = (url, method, data) => {
  return Axios({
    method: method,
    url: url,
    data: data,
  });
};

const getRandomArbitrary = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min);
};

const calculateCurrency = (price) => {
  let currency = getRandomArbitrary(20, 80);

  return { total: Math.floor(price * currency), currency: currency };
};

const formatNumberAsPrice = (unformattedPrice) => {
  var formatter = new Intl.NumberFormat("ru-RU", {
    style: "currency",
    currency: "RUB",

    // These options are needed to round to whole numbers if that's what you want.
    //minimumFractionDigits: 0,
    //maximumFractionDigits: 0,
  });

  return formatter.format(unformattedPrice);
};

export { sendRequestToBackend, calculateCurrency, formatNumberAsPrice };
