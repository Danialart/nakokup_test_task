import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "reactstrap";
import Cart from "../components/Cart";
import ProductCategory from "../components/ProductCategory";
import { calculateCurrency, sendRequestToBackend } from "../helpers/helpers";
const Goods = () => {
  const [names, setNames] = useState();
  const [data, setData] = useState();
  const [formattedProducts, setFormattedProducts] = useState([]);
  const [cartItems, setCartItems] = useState([]);

  // Run after component is rendered for the first time
  useEffect(() => {
    receiveItemsData();
    receiveNamesData();

    let interval = setInterval(() => {
      receiveItemsData();
    }, 15000);

    return () => {
      if (interval) clearInterval(interval);
    };
  }, []);

  const receiveItemsData = () => {
    sendRequestToBackend("/data/data.json", "get")
      .then((res) => res.data)
      .then((res) => {
        if (res.Success) setData(res.Value.Goods);
      });
  };

  const receiveNamesData = () => {
    sendRequestToBackend("/data/names.json", "get")
      .then((res) => res.data)
      .then((res) => {
        if (res) setNames(res);
      });
  };

  const formatItemsDataWithNames = () => {
    let result = [];

    data.map((item) => {
      let group = names[item.G];
      let product = group.B[item.T];

      let price_changed;
      let price = calculateCurrency(+item.C);
      let previousProduct = formattedProducts.find(
        (product) => product.product_id === +item.T
      );

      if (previousProduct)
        price_changed =
          previousProduct.price_rub < price.total ? "plus" : "minus";

      return result.push({
        product_id: item.T,
        group: {
          id: item.G,
          name: group.G,
        },
        title: product.N,
        remaining: item.P,
        price_rub: price.total,
        price_usd: +item.C,
        currency: price.currency,
        price_changed: price_changed,
      });
    });

    setFormattedProducts(result);
  };

  const updateCartItemCount = (product_id, newCount) => {
    setCartItems((cartItems) => {
      let cartItemsDestructurized = [...cartItems];
      let cartItemIndex = cartItems.findIndex(
        (product) => product.id === product_id
      );

      if (~cartItemIndex)
        cartItemsDestructurized[cartItemIndex] = {
          ...cartItemsDestructurized[cartItemIndex],
          quantity: newCount,
        };

      return cartItemsDestructurized;
    });
  };

  const addToCart = (product_id) => {
    // Check if product is the same - just add quantity, if not - add to cart
    setCartItems((actualItems) => {
      let updatedProducts = [...actualItems];
      let productInCartIndex = actualItems.findIndex(
        (product) => product.id === product_id
      );

      if (~productInCartIndex) {
        let product = updatedProducts[productInCartIndex];

        updatedProducts[productInCartIndex] = {
          ...product,
          quantity: product.quantity + 1,
        };

        return updatedProducts;
      } else
        updatedProducts.push({
          id: product_id,
          quantity: 1,
        });

      return updatedProducts;
    });
  };

  const removeFromCart = (product_id) => {
    setCartItems((items) =>
      [...items].filter((item) => item.id !== product_id)
    );
  };

  // When we got a new data in the data or names state variable
  useEffect(() => {
    if (data && names) formatItemsDataWithNames();
  }, [data, names]);

  return (
    <Container>
      <div className="Shop-items my-4 w-100">
        <h1>Категории и товары</h1>
        <Row>
          <Col>
            {formattedProducts && formattedProducts.length ? (
              <div className="categories-list w-100">
                {Object.keys(names).map((catid, k) => {
                  let category = names[catid];

                  return (
                    <ProductCategory
                      products={formattedProducts}
                      category={category}
                      catid={catid}
                      key={k}
                      addToCart={addToCart}
                    />
                  );
                })}
              </div>
            ) : null}
          </Col>
          {cartItems.length ? (
            <Col xs="12" className="mt-5">
              <Cart
                cartItems={cartItems}
                removeFromCart={removeFromCart}
                productsList={formattedProducts}
                updateCartItemCount={updateCartItemCount}
              />
            </Col>
          ) : null}
        </Row>
      </div>
    </Container>
  );
};

export default Goods;
