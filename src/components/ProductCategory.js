import React from "react";
import { formatNumberAsPrice } from "../helpers/helpers";
const { useState } = require("react");

const ProductCategory = ({ products, category, addToCart, catid }) => {
  const [opened, setOpened] = useState(false);

  return (
    <div className="category-item w-100">
      <div
        className="collapse-trigger cursor-pointer"
        onClick={() => setOpened(!opened)}
      >
        {category.G}
      </div>

      <div className={`products-list ${opened ? "d-block" : "d-none"}`}>
        {products.filter((product) => product.group.id === +catid).length ? (
          products
            .filter((product) => product.group.id === +catid)
            .map((product) => (
              <div
                className="product-item d-flex align-items-center justify-content-between"
                key={product.product_id}
              >
                <a
                  href="/"
                  onClick={(e) => {
                    e.preventDefault();
                    addToCart(product.product_id);
                  }}
                >
                  {product.title} ({product.remaining})
                </a>
                <div
                  className={`price-container ${
                    product.price_changed && product.price_changed === "plus"
                      ? "price-red"
                      : product.price_changed
                      ? "price-green"
                      : ""
                  }`}
                >
                  <a
                    href="/"
                    onClick={(e) => {
                      e.preventDefault();
                      addToCart(product.product_id);
                    }}
                  >
                    <strong className="price d-block">
                      {formatNumberAsPrice(product.price_rub)}
                    </strong>
                    <small>
                      Цена в USD: {product.price_usd}$<br />
                      Курс: {product.currency}
                    </small>
                  </a>
                </div>
              </div>
            ))
        ) : (
          <p className="p-3">Нет доступных товаров для этой категории</p>
        )}
      </div>
    </div>
  );
};

export default ProductCategory;
