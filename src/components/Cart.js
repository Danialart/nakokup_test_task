import React from "react";
import { Input, Table } from "reactstrap";
import { formatNumberAsPrice } from "../helpers/helpers";

const Cart = ({
  cartItems,
  updateCartItemCount,
  removeFromCart,
  productsList,
}) => {
  let totalSumm = 0;

  if (cartItems.length)
    cartItems.map((item) => {
      let product = productsList.find(
        (product) => item.id === product.product_id
      );

      return (totalSumm += product.price_rub * item.quantity);
    });

  return (
    <div className="cart">
      <h4>Ваша корзина</h4>
      <Table bordered className="product-items">
        <thead>
          <tr>
            <td>Наименование товара</td>
            <td>Цена</td>
            <td width="15%">Количество</td>
            <td>Общая стоимость</td>
            <td></td>
          </tr>
        </thead>
        <tbody>
          {cartItems.map((cartItem) => {
            let product = productsList.find(
              (product) => product.product_id === cartItem.id
            );

            return (
              <tr className="product-item" key={cartItem.id}>
                <td className="product-name">{product.title}</td>
                <td className="price">
                  {formatNumberAsPrice(product.price_rub)}
                </td>
                <td className="d-flex align-items-center">
                  <Input
                    type="number"
                    className="text-center price-input mx-2"
                    onChange={(e) =>
                      updateCartItemCount(product.product_id, +e.target.value)
                    }
                    value={cartItem.quantity}
                  />
                  <span>шт.</span>
                </td>
                <td>
                  {formatNumberAsPrice(cartItem.quantity * product.price_rub)}
                </td>
                <td className="text-center d-flex align-items-center justify-content-center">
                  <a
                    href="/"
                    onClick={(e) => {
                      e.preventDefault();
                      removeFromCart(product.product_id);
                    }}
                  >
                    <svg
                      height="16"
                      viewBox="0 0 365.71733 365"
                      width="16"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g fill="#f44336">
                        <path d="m356.339844 296.347656-286.613282-286.613281c-12.5-12.5-32.765624-12.5-45.246093 0l-15.105469 15.082031c-12.5 12.503906-12.5 32.769532 0 45.25l286.613281 286.613282c12.503907 12.5 32.769531 12.5 45.25 0l15.082031-15.082032c12.523438-12.480468 12.523438-32.75.019532-45.25zm0 0" />
                        <path d="m295.988281 9.734375-286.613281 286.613281c-12.5 12.5-12.5 32.769532 0 45.25l15.082031 15.082032c12.503907 12.5 32.769531 12.5 45.25 0l286.632813-286.59375c12.503906-12.5 12.503906-32.765626 0-45.246094l-15.082032-15.082032c-12.5-12.523437-32.765624-12.523437-45.269531-.023437zm0 0" />
                      </g>
                    </svg>
                  </a>
                </td>
              </tr>
            );
          })}
          <tr>
            <td colSpan="4">
              <strong>Общая стоимость заказа:</strong>{" "}
              {formatNumberAsPrice(totalSumm)}
            </td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default Cart;
