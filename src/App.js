import React from "react";
import { Link, Route, BrowserRouter, Switch } from "react-router-dom";

import Goods from "./pages/Goods";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.less";
import { Container, Row } from "reactstrap";

function App() {
  return (
    <BrowserRouter>
      <nav className="top-navigation">
        <Container>
          <Row>
            <ul className="list-container w-100">
              <li className="list-item">
                <Link className="link-item" to="/">
                  Товары
                </Link>
              </li>
            </ul>
          </Row>
        </Container>
      </nav>

      <Switch>
        <Route path="/">
          <Goods />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
